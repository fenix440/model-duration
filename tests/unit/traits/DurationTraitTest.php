<?php
use Fenix440\Model\Duration\Traits\DurationTrait;
use Fenix440\Model\Duration\Interfaces\DurationAware;

/**
 * Class DurationTraitTest
 *
 * @coversDefaultClass Fenix440\Model\Duration\Traits\DurationTrait
 * @author Bartlomiej Szala <fenix440@gmail.com>
 */
class DurationTraitTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /************************************************************************
     * Data "providers"
     ***********************************************************************/

    /**
     * Get the trait mock
     *
     * @return PHPUnit_Framework_MockObject_MockObject|Fenix440\Model\Duration\Interfaces\DurationAware
     */
    protected function getTraitMock()
    {
        return $this->getMockForTrait('Fenix440\Model\Duration\Traits\DurationTrait');
    }

    /**
     * Get date interval Format
     * @return string
     */
    protected function getDateIntervalFormat(){
        return "%d days, %h hours %I minutes and %S seconds";
    }

    /************************************************************************
     * Actual tests
     ***********************************************************************/

    /**
     * @test
     * @covers  ::setDuration
     * @covers  ::isDurationValid
     * @covers  ::getDuration
     * @covers  ::getDefaultDuration
     * @covers  ::hasDefaultDuration
     * @covers  ::hasDuration
     */
    public function setAndGetDuration()
    {
        $trait = $this->getTraitMock();
        $durationFormat = "P2DT3H25M10S"; // two days 3 hours 25 minutes and 10 seconds
        $duration = new DateInterval($durationFormat);
        $trait->setDuration($duration);

        $this->assertSame($duration, $trait->getDuration(),'Duration is invalid');
        $this->assertInstanceOf('\DateInterval',$trait->getDuration(),'Duration has wrong type!');
        $this->assertSame($duration->format($this->getDateIntervalFormat()),$trait->getDuration()->format($this->getDateIntervalFormat()),'Date interval format is not the same');

    }

    /**
     * @test
     * @covers  ::setDuration
     * @covers  ::isDurationValid
     * @expectedException   \Fenix440\Model\Duration\Exceptions\InvalidDurationException
     */
    public function setInvalidDuration()
    {
        $trait = $this->getTraitMock();
        $duration = new DateTime();

        $trait->setDuration($duration);
    }

    /**
     * @test
     * @covers  ::getDuration
     * @covers  ::getDefaultDuration
     * @covers  ::hasDefaultDuration
     * @covers  ::hasDuration
     */
    public function testDefaultDuration()
    {
        $trait = $this->getTraitMock();

        $this->assertNull($trait->getDuration(),'Default duration is not null!');
    }

}