<?php
use Fenix440\Model\Duration\Validators\DurationValidator;

/**
 * Class DurationTraitTest
 *
 * @coversDefaultClass Fenix440\Model\Duration\Validators\DurationValidator
 * @author Bartlomiej Szala <fenix440@gmail.com>
 */
class DurationValidatorTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }


    /************************************************************************
     * Actual tests
     ***********************************************************************/

    /**
     * @test
     * @covers  ::isValid
     */
    public function isDurationValid()
    {
        $durationFormat = "P2DT3H25M10S"; // two days 3 hours 25 minutes and 10 seconds
        $duration = new DateInterval($durationFormat);

        $this->assertTrue(DurationValidator::isValid($duration),'Duration is invalid');
    }

    /**
     * @test
     * @covers  ::isValid
     */
    public function isDurationInvalid()
    {
        $this->assertFalse(DurationValidator::isValid(null),'Duration is valid ');
    }
}

