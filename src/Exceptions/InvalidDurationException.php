<?php  namespace Fenix440\Model\Duration\Exceptions; 

/**
 * Class InvalidDurationException
 *
 * Throws and exception if duration is invalid
 *
 * @see DurationAware
 *
 * @package Fenix440\Model\Duration\Exceptions 
 * @author      Bartlomiej Szala <fenix440@gmail.com>
*/
class InvalidDurationException extends \InvalidArgumentException{

 

}

 