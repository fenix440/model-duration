<?php  namespace Fenix440\Model\Duration\Traits;
use Fenix440\Model\Duration\Exceptions\InvalidDurationException;
use Aedart\Validate\Number\Integer\UnsignedIntegerValidator;
use Fenix440\Model\Duration\Validators\DurationValidator;

/**
 * Trait DurationTrait
 *
 * @see DurationAware
 *
 * @package      Fenix440\Model\Duration\Traits
 * @author      Bartlomiej Szala <fenix440@gmail.com>
*/
trait DurationTrait {

    /**
     * Duration for a given component
     *
     * @var null|\DateInterval
     */
    protected $duration=null;

    /**
     * Set duration for a given component
     *
     * @param \DateInterval $duration Duration for a given component
     * @return void
     * @throws InvalidDurationException If duration is invalid
     */
    public function setDuration($duration){
        if(!$this->isDurationValid($duration))
            throw new InvalidDurationException(sprintf('Duration %d is invalid',var_export($duration,true)));
        $this->duration=$duration;
    }

    /**
     * Validates if duration is valid
     * @param mixed $duration   Duration for given component
     * @return bool true/false
     */
    public function isDurationValid($duration){
        return DurationValidator::isValid($duration);
    }

    /**
     * Get duration
     *
     * @return \DateInterval|null
     */
    public function getDuration(){
        if(!$this->hasDuration() && $this->hasDefaultDuration())
            $this->setDuration($this->getDefaultDuration());
        return $this->duration;
    }

    /**
     * Get default duration
     *
     * @return \DateInterval|null
     */
    public function getDefaultDuration(){
        return null;
    }

    /**
     * Checks if default duration is set
     *
     * @return bool true/false
     */
    public function hasDefaultDuration(){
        return (!is_null($this->getDefaultDuration()))? true:false;
    }

    /**
     * Check if duration is set
     *
     * @return bool true/false
     */
    public function hasDuration(){
        return (!is_null($this->duration))? true:false;
    }

}