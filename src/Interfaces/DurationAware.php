<?php namespace Fenix440\Model\Duration\Interfaces;
use Fenix440\Model\Duration\Exceptions\InvalidDurationException;

/**
 * Interface DurationAware
 *
 * A component/object must know about duration property
 * Note:
 * Duration property will store values as DateInterval object
 *
 * @see http://php.net/manual/en/class.dateinterval.php
 * @see http://php.net/manual/en/datetime.diff.php
 *
 * @author      Bartlomiej Szala <fenix440@gmail.com>
 * @package      Fenix440\Model\Duration\Interfaces
*/
interface DurationAware {

    /**
     * Set duration for a given component
     *
     * @param \DateInterval $duration Duration for a given component
     * @return void
     * @throws InvalidDurationException If duration is invalid
     */
    public function setDuration($duration);

    /**
     * Validates if duration is valid
     * @param mixed $duration   Duration for given component
     * @return bool true/false
     */
    public function isDurationValid($duration);

    /**
     * Get duration
     *
     * @return \DateInterval|null
     */
    public function getDuration();

    /**
     * Get default duration
     *
     * @return \DateInterval|null
     */
    public function getDefaultDuration();

    /**
     * Checks if default duration is set
     *
     * @return bool true/false
     */
    public function hasDefaultDuration();

    /**
     * Check if duration is set
     *
     * @return bool true/false
     */
    public function hasDuration();

}