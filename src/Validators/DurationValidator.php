<?php  namespace Fenix440\Model\Duration\Validators;

use DateInterval;
use Fenix440\Model\Duration\Validators\Interfaces\Validator;

/**
 * Class DurationValidator
 *
 * This object validates if duration has valid value
 * @see \Fenix440\Model\Duration\Validators\Interfaces\Validator
 *
 * @package Fenix440\Model\Duration\Validators 
 * @author      Bartlomiej Szala <fenix440@gmail.com>
*/
class DurationValidator implements Validator{


    /**
     * Check if the given value is valid or not
     *
     * @param mixed $value Value to be tested if its valid or not
     *
     * @return boolean True if value is valid, false if not
     */
    public static function isValid($value)
    {
        return ($value instanceof DateInterval)? true:false;
    }
}

 