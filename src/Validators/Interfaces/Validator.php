<?php namespace Fenix440\Model\Duration\Validators\Interfaces;

/**
 * Interface Validator
 *
 * A validator must ensure to test a given testable value (data, object or set thereof), and return
 * <b>true if and only if validation requirements has passed</b>. Otherwise, the validator
 * must return false.
 *
 * Furthermore, any implementation should always assume that data is invalid by default, and
 * never return true in cases where there might be any kind of doubt!
 *
 * This interface has been inspired by the Zend\Validator\ValidatorInterface
 *
 * @author Bartlomiej Szala <fenix440@gmail.com>
 * @package Fenix440\Model\Duration\Validators\Interfaces
 */
interface Validator {

    /**
     * Check if the given value is valid or not
     *
     * @param mixed $value Value to be tested if its valid or not
     *
     * @return boolean True if value is valid, false if not
     */
    public static function isValid($value);

}